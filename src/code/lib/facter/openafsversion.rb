# Fact: openafsversion
#
# Purpose: Report the version of openafs
#
# frozen_string_literal: true

Facter.add(:openafsversion) do
  setcode do
    Facter::Util::Resolution.exec('/bin/rpm -q --queryformat "%{VERSION}-%{RELEASE}" openafs')
  rescue StandardError
    Facter.debug('openafs not available')
  end
end
