# Fact: openafspartsize
#
# Purpose: Report the size of the volume that /usr/vice/cache resides on.
#
# frozen_string_literal: true

if Facter.value(:kernel) == 'Linux' && File.exist?('/usr/vice/cache')
  Facter.add(:openafspartsize) do
    setcode do
      Facter::Util::Resolution.exec('/bin/df -k -P /usr/vice/cache').split("\n").last.split(%r{\s+})[1].to_i
    rescue StandardError
      Facter.debug('Error getting /usr/vice/cache partition size')
    end
  end
end
