# frozen_string_literal: true

require 'spec_helper'

describe 'afs', type: 'class' do
  on_supported_os.each do |os, facts|
    context "on #{os}" do
      let(:facts) do
        facts
      end
      let(:hiera_config) { 'spec/hiera.yaml' }

      context 'with default options,' do
        it { is_expected.to compile.with_all_deps }
        it { is_expected.to contain_package('openafs') }
        it { is_expected.to contain_package('kmod-openafs') }
        it { is_expected.to contain_package('openafs-client') }
        it { is_expected.to contain_package('openafs-krb5') }
        it { is_expected.to contain_package('cern-wrappers') }
        it { is_expected.to contain_service('openafs-client') }
        it { is_expected.to contain_file('/etc/sysconfig/openafs').with_content(%r{^AFS_POST_INIT="?/usr/bin/.*$}) }
        it { is_expected.to contain_file('/usr/vice/etc/ThisCell').with_content(%r{^cern.ch$}) }
        it { is_expected.to contain_systemd__unit_file('openafs-client.service').with_content(%r{Requires=network-online.target}) }
        it { is_expected.to contain_systemd__unit_file('openafs-client.service').with_content(%r{^ExecStartPost=/usr/bin/fs setcrypt}) }
        it { is_expected.not_to contain_package('dkms-openafs') }
        it { is_expected.not_to contain_package('afs_tools') }
        it { is_expected.to contain_file('/etc/sysconfig/modules/openafs-client.modules').with_ensure('absent') }

        case facts[:os]['release']['major']
        when '7'
          it { is_expected.not_to contain_class('nftables') }
          it { is_expected.to have_pam_resource_count(0) }
          it { is_expected.to contain_firewall('100 allow afs3-callback') }
          it { is_expected.not_to contain_class('afs::kojitag') }
          it { is_expected.not_to contain_osrepos__kojitag('openafs') }
          it { is_expected.to contain_package('pam_krb5') }
          it { is_expected.not_to contain_package('pam_afs_session') }
          it { is_expected.not_to contain_package('cern-aklog-systemd-user') }
          it { is_expected.not_to contain_file('/usr/local/bin/aklog.sh') }
        else
          it { is_expected.to have_firewall_resource_count(0) }
          it { is_expected.to contain_class('afs::kojitag') }
          it { is_expected.to contain_osrepos__kojitag('openafs') }
          it { is_expected.not_to contain_package('pam_krb5') }
          it { is_expected.to contain_file('/usr/local/bin/aklog.sh').with_content(%r{^/usr/bin/systemd-cat -t aklog.sh /usr/bin/aklog \$@$}) }
          it { is_expected.to contain_package('pam_afs_session') }
          it { is_expected.to contain_pam('Add pam_afs_session to ssh stack') }
          it { is_expected.to contain_package('cern-aklog-systemd-user').with_ensure('absent') }
        end
      end

      context 'with memcache option set,' do
        let(:params) do
          {
            options: { 'memcache' => true }
          }
        end

        it { is_expected.to compile.with_all_deps }
        it { is_expected.to contain_file('/etc/sysconfig/openafs').with_content(%r{^AFSD_ARGS=".*-memcache .*"$}) }

        context 'memcachesize as string and too-small,' do
          let(:params) do
            super().merge({ memcachesize: '1234' })
          end

          it { is_expected.to contain_file('/usr/vice/etc/cacheinfo').with_content(%r{^/afs:/usr/vice/cache:10240$}) }
        end

        context 'memcachesize as int and too small,' do
          let(:params) do
            super().merge({ memcachesize: 5435 })
          end

          it { is_expected.to contain_file('/usr/vice/etc/cacheinfo').with_content(%r{^/afs:/usr/vice/cache:10240$}) }
        end

        context 'memcachesize as string and okay,' do
          let(:params) do
            super().merge({ memcachesize: '12345' })
          end

          it { is_expected.to contain_file('/usr/vice/etc/cacheinfo').with_content(%r{^/afs:/usr/vice/cache:12345$}) }
        end

        context 'memcachesize as int and okay,' do
          let(:params) do
            super().merge({ memcachesize: 54_321 })
          end

          it { is_expected.to contain_file('/usr/vice/etc/cacheinfo').with_content(%r{^/afs:/usr/vice/cache:54321$}) }
        end
      end

      context 'with cachesize option set' do
        context 'as string, and too small' do
          let(:params) do
            {
              cachesize: '1234'
            }
          end

          it { is_expected.to compile.with_all_deps }
          it { is_expected.to contain_file('/etc/sysconfig/openafs').without_content(%r{^AFSD_ARGS=".*-cache .*"$}) }
          it { is_expected.to contain_file('/usr/vice/etc/cacheinfo').with_content(%r{^/afs:/usr/vice/cache:131072$}) }
        end

        context 'as int, and too small' do
          let(:params) do
            {
              cachesize: 5435
            }
          end

          it { is_expected.to compile.with_all_deps }
          it { is_expected.to contain_file('/etc/sysconfig/openafs').without_content(%r{^AFSD_ARGS=".*-memcache .*"$}) }
          it { is_expected.to contain_file('/usr/vice/etc/cacheinfo').with_content(%r{^/afs:/usr/vice/cache:131072$}) }
        end

        context 'as string, and okay' do
          let(:params) do
            {
              cachesize: '9876543'
            }
          end

          it { is_expected.to compile.with_all_deps }
          it { is_expected.to contain_file('/etc/sysconfig/openafs').without_content(%r{^AFSD_ARGS=".*-memcache .*"$}) }
          it { is_expected.to contain_file('/usr/vice/etc/cacheinfo').with_content(%r{^/afs:/usr/vice/cache:9876543$}) }
        end

        context 'as int, and okay' do
          let(:params) do
            {
              cachesize: 9_876_543
            }
          end

          it { is_expected.to compile.with_all_deps }
          it { is_expected.to contain_file('/etc/sysconfig/openafs').without_content(%r{^AFSD_ARGS=".*-memcache .*"$}) }
          it { is_expected.to contain_file('/usr/vice/etc/cacheinfo').with_content(%r{^/afs:/usr/vice/cache:9876543$}) }
        end

        context 'as AUTOMATIC, i.e 0.8 of the openafspartsize fact' do
          let(:params) do
            {
              cachesize: 'AUTOMATIC'
            }
          end
          let(:facts) do
            super().merge(
              openafspartsize: 1_000_000
            )
          end

          it { is_expected.to compile.with_all_deps }
          it { is_expected.to contain_file('/etc/sysconfig/openafs').without_content(%r{^AFSD_ARGS=".*-memcache .*"$}) }
          it { is_expected.to contain_file('/usr/vice/etc/cacheinfo').with_content(%r{^/afs:/usr/vice/cache:800000$}) }
        end

        context 'with cachesize AUTOMATIC 0.8 and no openafspartsize fact' do
          let(:params) do
            {
              cachesize: 'AUTOMATIC'
            }
          end

          it { is_expected.to compile.with_all_deps }
          it { is_expected.to contain_file('/etc/sysconfig/openafs').without_content(%r{^AFSD_ARGS=".*-memcache .*"$}) }
          it { is_expected.to contain_file('/usr/vice/etc/cacheinfo').with_content(%r{^/afs:/usr/vice/cache:131072$}) }
        end
      end

      context 'with enable_firewall false' do
        let(:params) do
          {
            enable_firewall: false
          }
        end

        it { is_expected.to compile.with_all_deps }
        it { is_expected.to have_firewall_resource_count(0) }
        it { is_expected.not_to contain_class('nftables') }
      end

      context 'with cernfw::manager set to iptables' do
        let(:pre_condition) { 'class{"cernfw": manager => "iptables"}' }

        it { is_expected.to contain_firewall('100 allow afs3-callback') }
        it { is_expected.not_to contain_class('nftables::rules::afs3_callback') }
      end

      context 'with cernfw::manager set to nftables' do
        let(:pre_condition) { 'class{"cernfw": manager => "nftables"}' }

        it { is_expected.to have_firewall_resource_count(0) }
        it { is_expected.to contain_class('nftables::rules::afs3_callback') }
      end

      context 'with aklog_unit and pam_afs_session true' do
        let(:params) do
          {
            aklog_unit: true,
            pam_afs_session: true
          }
        end

        it { is_expected.not_to contain_pam('Remove pam_afs_session to ssh stack') }

        case facts[:os]['release']['major']
        when '7'
          it { is_expected.not_to contain_package('pam_afs_session') }
          it { is_expected.not_to contain_pam('Add pam_afs_session to ssh stack') }
          it { is_expected.not_to contain_package('cern-aklog-systemd-user') }
          it { is_expected.not_to contain_exec('enable_aklog_service') }
        else
          it { is_expected.to contain_package('pam_afs_session').with_ensure('present') }
          it { is_expected.to contain_pam('Add pam_afs_session to ssh stack') }
          it { is_expected.to contain_package('cern-aklog-systemd-user').with_ensure('present') }
          it { is_expected.to contain_exec('enable_aklog_service') }
          it { is_expected.to contain_file('/usr/local/bin/aklog.sh').with_content(%r{^/usr/bin/systemd-cat -t aklog.sh /usr/bin/aklog \$@$}) }
        end
        context 'with pam_afs_session_debug false' do
          let(:params) { super().merge(pam_afs_session_debug: false) }

          case facts[:os]['release']['major']
          when '7'
            it { is_expected.not_to contain_file('/usr/local/bin/aklog.sh') }
          else
            it { is_expected.to contain_file('/usr/local/bin/aklog.sh').with_content(%r{^/usr/bin/systemd-cat -t aklog.sh /usr/bin/aklog \$@$}) }
          end
        end

        context 'with pam_afs_session_debug true' do
          let(:params) { super().merge(pam_afs_session_debug: true) }

          case facts[:os]['release']['major']
          when '7'
            it { is_expected.not_to contain_file('/usr/local/bin/aklog.sh') }
          else
            it { is_expected.to contain_file('/usr/local/bin/aklog.sh').with_content(%r{^/usr/bin/systemd-cat -t aklog.sh /usr/bin/aklog -d \$@$}) }
          end
        end
      end

      context 'with aklog_unit and pam_afs_session false' do
        let(:params) do
          {
            aklog_unit: false,
            pam_afs_session: false
          }
        end

        it { is_expected.not_to contain_exec('enable_aklog_service') }
        it { is_expected.not_to contain_pam('Add pam_afs_session to ssh stack') }

        case facts[:os]['release']['major']
        when '7'
          it { is_expected.not_to contain_pam('Remove pam_afs_session to ssh stack') }
          it { is_expected.not_to contain_package('pam_afs_session') }
          it { is_expected.not_to contain_package('cern-aklog-systemd-user') }
        else
          it { is_expected.to contain_package('pam_afs_session').with_ensure('absent') }
          it { is_expected.to contain_package('cern-aklog-systemd-user').with_ensure('absent') }
          it { is_expected.to contain_pam('Remove pam_afs_session to ssh stack') }
        end
      end

      context 'with handle_cellservdb not set' do
        it { is_expected.to contain_file('/usr/vice/etc/CellServDB.dist') }
        it { is_expected.to contain_file('/usr/vice/etc/CellServDB.local') }
      end

      context 'with handle_cellservdb false' do
        let(:params) do
          {
            handle_cellservdb: false
          }
        end

        it { is_expected.not_to contain_file('/usr/vice/etc/CellServDB.dist') }
        it { is_expected.not_to contain_file('/usr/vice/etc/CellServDB.local') }
      end

      context 'with thiscell option set,' do
        let(:params) do
          {
            thiscell: 'othercell.foo'
          }
        end

        it { is_expected.to compile.with_all_deps }
        it { is_expected.to contain_file('/usr/vice/etc/ThisCell').with_content(%r{^othercell.foo$}) }
      end

      context 'with selinux module for sssd' do
        let(:facts) do
          super().merge({ selinux_config_policy: 'targeted' })
        end

        case facts[:os]['release']['major']
        when '7'
          it { is_expected.not_to contain_selinux__module('openafs-client-sssd') }
        else
          it { is_expected.to contain_selinux__module('openafs-client-sssd') }
        end
      end

      context 'with use_osrepos=false,' do
        let(:params) do
          {
            use_osrepos: false
          }
        end

        it { is_expected.not_to contain_class('afs::kojitag') }
        it { is_expected.not_to contain_osrepos__kojitag('openafs') }
      end

      context 'with prefer_upstream_dkms true' do
        let(:params) { { prefer_upstream_dkms: true } }

        it { is_expected.to contain_package('dkms-openafs') }
        it { is_expected.not_to contain_package('kmod-openafs') }
        it { is_expected.not_to contain_package('afs_tools') }

        context 'with tools true' do
          let(:params) do
            super().merge({ tools: true })
          end

          it {
            is_expected.to contain_package('afs_tools').
              with_ensure('installed').
              that_requires('Package[dkms-openafs]')
          }
        end
      end

      context 'with prefer_upstream_dkms false' do
        let(:params) { { prefer_upstream_dkms: false } }

        it { is_expected.to contain_package('kmod-openafs') }
        it { is_expected.not_to contain_package('dkms-openafs') }
        it { is_expected.not_to contain_package('afs_tools') }

        context 'with tools true' do
          let(:params) do
            super().merge({ tools: true })
          end

          it {
            is_expected.to contain_package('afs_tools').
              with_ensure('installed').
              that_requires('Package[kmod-openafs]')
          }
        end
      end

      context 'with setcrypt false' do
        let(:params) { { setcrypt: false } }

        it { is_expected.to contain_file('/etc/sysconfig/openafs').without_content(%r{^AFS_POST_INIT="?/usr/bin/fs setcrypt}) }
        it { is_expected.to contain_systemd__unit_file('openafs-client.service').without_content(%r{^ExecStartPost=/usr/bin/fs setcrypt}) }
      end
    end
  end
end
