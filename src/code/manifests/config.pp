# @summary configures afs packages
# @api private
class afs::config(
  Variant[Enum['AUTOMATIC'], Pattern[/\A\d+\z/], Integer[0]] $cachesize = $afs::cachesize,
  Variant[Pattern[/\A\d+\z/], Integer[0]] $memcachesize = $afs::memcachesize,
  Hash $options   = $afs::options,
  Enum['on','off'] $runserver = $afs::runserver,
  String[1] $thiscell = $afs::thiscell,
  Boolean $pam_afs_session = $afs::pam_afs_session,
  Boolean $pam_afs_session_debug = $afs::pam_afs_session_debug,
  Boolean $aklog_unit = $afs::aklog_unit,
  Boolean $handle_cellservdb = $afs::handle_cellservdb,
  Boolean $setcrypt = $afs::setcrypt,
) {

  # Merge with hiera hash first and with default options hash afterwards
  $tmp_merged_options = merge($afs::params::default_options, $afs::params::options, $options)
  if $tmp_merged_options['memcache'] {
    $merged_options = delete(delete(delete(delete($tmp_merged_options, 'stat'), 'files'), 'dcache'), 'chunksize')
  } else {
    $merged_options = $tmp_merged_options
  }

  if($handle_cellservdb) {  # not applicable for test cells.
    file{'/usr/vice/etc/CellServDB.local':
      ensure    => file,
      owner     => 'root',
      group     => 'root',
      mode      => '0644',
      show_diff => false,  # workaround for https://tickets.puppetlabs.com/browse/PUP-1441
      source    => 'puppet:///modules/afs/CellServDB.local',
    }
    file{'/usr/vice/etc/CellServDB.dist':
      ensure    => file,
      owner     => 'root',
      group     => 'root',
      mode      => '0644',
      show_diff => false,  # workaround for https://tickets.puppetlabs.com/browse/PUP-1441
      source    => 'puppet:///modules/afs/CellServDB.dist',
    }
  }

  file{'/usr/vice/etc/ThisCell':
    ensure  => 'file',
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => "${thiscell}\n",
  }

  if $merged_options['memcache'] {
    if Integer.new($memcachesize) > 4194304 { # max mem cache size is 4GB
      $my_cachesize = 4194304
    } elsif Integer.new($memcachesize) < 10240 { # min cache size is 10MB
      $my_cachesize = 10240
    } else {
      $my_cachesize = $memcachesize
    }
  } else {
    if $cachesize == 'AUTOMATIC' {
      if $facts['openafspartsize'] {
        $my_cachesize = Integer(0.8 * $facts['openafspartsize'])
      } else {
        $my_cachesize = 131072 # sensible default
      }
    } else {
      if Integer.new($cachesize) > 52428800 { # max cache size is 50GB
        $my_cachesize = 52428800
      } elsif Integer.new($cachesize) < 131072 { # min cache size is 128MB
        $my_cachesize = 131072
      } else {
        $my_cachesize = $cachesize
      }
    }
  }

  file{'/etc/sysconfig/openafs':
    ensure  => 'file',
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => template('afs/sysconfig-afs.erb'),
  }

  file{'/usr/vice/etc/cacheinfo':
    ensure  => 'file',
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => "/afs:/usr/vice/cache:${my_cachesize}\n",
  }

  # we overwrite the RPM-provided file until upstream has a *hard*
  # dependency on the network being up
  systemd::unit_file{'openafs-client.service':
    content => template('afs/openafs-client.service.erb'),
  }

  if (versioncmp($facts['os']['release']['major'], '8') >= 0) and $aklog_unit {
    exec{'enable_aklog_service':
      command => '/usr/bin/systemctl --user --global enable aklog.timer',
      unless  => '/usr/bin/systemctl is-enabled --user --global aklog.timer',
    }
  } # No need to disable as package removal should handle that.

  if (versioncmp($facts['os']['release']['major'], '8') >= 0)  {
    # adding 'optional => true' below would better but:
    # https://github.com/hercules-team/augeasproviders_pam/issues/27
    if $pam_afs_session {
      file{'/usr/local/bin/aklog.sh':
        ensure  => file,
        mode    => '0755',
        owner   => root,
        group   => root,
        content => epp('afs/aklog.sh', {'debug' => $pam_afs_session_debug}),
      }
      pam{'Add pam_afs_session to ssh stack':
        ensure    => 'present',
        service   => 'sshd',
        type      => 'session',
        control   => 'optional',
        module    => 'pam_afs_session.so',
        position  => 'before module postlogin',
        arguments => ['always_aklog', 'debug','program=/usr/local/bin/aklog.sh'],
        require   => File['/usr/local/bin/aklog.sh'],
      }
    } else {
      pam{'Remove pam_afs_session to ssh stack':
        ensure   => 'absent',
        service  => 'sshd',
        optional => true,
        control  => 'optional',
        type     => 'session',
        module   => 'pam_afs_session.so',
      }
    }

    # CS8/CS9 can use 'sssd' for resolving services and hosts,
    # 'afsd' might need to be able to talk to /var/lib/sss/pipes/nss
    # (actual fallthrough to files/dns seems to work in general, though)
    selinux::module {'openafs-client-sssd':
        ensure     => present,
        content_te => file('afs/openafs-client-sssd.te'),
    }
  }
}
