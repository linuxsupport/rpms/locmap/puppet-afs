# @summary Maintain firewall entries
# @api private
class afs::firewall {
  if str2bool($::writefirewall)  {
    include cernfw
    case $cernfw::manager {
      'iptables': {
        firewall {'100 allow afs3-callback':
          proto  => 'udp',
          dport  => '7001',
          action => 'accept',
        }
      }
      'nftables': {
        include nftables::rules::afs3_callback
      }
      default: {
        fail('Unknown cernfw::manager setting')
      }
    }
  }
}

