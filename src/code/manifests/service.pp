# @summary Control afs sserice
# @api private
class afs::service {
  service { 'openafs-client':
    ensure     => running,
    hasstatus  => false,    # unreliable on CC7 systemd
    pattern    => ' \[afsd\]$', # cache manager kernel thread, shows as '[afsd]' in "ps -ef"
    hasrestart => false,    # generally a very bad idea
    stop       => '/bin/false',
    enable     => true,
  }
}
