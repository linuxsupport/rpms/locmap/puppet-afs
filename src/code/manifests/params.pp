# @summary Defaults for afs module
# @api private
class afs::params {

  $cachesize = lookup('afs_cachesize', Variant[String,Integer[0]], 'first', 524288)
  $memcachesize = lookup('afs_memcachesize', Variant[String,Integer[0]], 'first', 262144)
  $default_options = {
    chunksize   => 18,
    daemons     => 10,
    dcache      => 60000,
    fakestat    => true,
    files       => 50000,
    stat        => 70000,
    volumes     => 512,
    memcache    => false,
    dynroot     => false,
    afsdb       => true,
  }
  $options   = lookup('afs_options',Hash,{'strategy' => 'deep', 'merge_hash_arrays' =>  true},{})
  $runserver = lookup('afs_runserver',Enum['on','off'],'first','off')
  $thiscell  = lookup('afs_thiscell',String[1], 'first', 'cern.ch')

  $_openafsversion = getvar(::openafsversion)
  $enable_firewall = true
  $prefer_upstream_dkms = false  # choice between kmod-openafs (weak symbols) or dkms-openafs (recompiled on the machine)

  $aklog_unit = false
  $pam_afs_session = true
  $handle_cellservdb = true
  $use_osrepos = true
}
