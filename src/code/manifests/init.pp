# @summary  Install and configure AFS
#
# @example Simple Example
#  include afs
#
# @example Use AFS Cache in Memory
#  class{'afs':
#    memcachesize => 524288,
#    options      => {
#      memcache => true,
#    },
#  }
#
# @example Enable aklog's -d debug option
#  class{'afs':
#    pam_afs_session_debug = true,
#   }
#
# @param thiscell
#  name of the AFS cell this client belongs to
# @param cachesize disk cache size
# @param memcachesize memorycachesize
# @param options
#  Hash of permissable TDB
# @param enable_firewall
#  open AFS client callback port (UDP/7001) in the firewall
# @param runserver run an afs server or not
# @param prefer_upstream_dkms use dkms module
# @param aklog_unit
#  install and enable systemd user timer aklog.timer C8 and newer only.
# @param pam_afs_session
#  install and configure pam_afs_session in sshd pamstack. C8 and newer only.
# @param pam_afs_session_debug
#  If true aklog will be called  with the -d option
# @param handle_cellservdb
#  let the module handle the client CellServDB files. On by default.
# @param use_osrepos
#  allows to _not_ rely on the 'openafs' KOJI repository
# @param tools Install afs_tools/afs_tools_standalone package, EL7 and 8 only.
# @param setcrypt Turn on AFS traffic "encryption" ("based on but slightly weaker than DES"), on by default.
#
class afs (
  Variant[Enum['AUTOMATIC'], Pattern[/\A\d+\z/], Integer[0]] $cachesize = $afs::params::cachesize,
  Variant[Pattern[/\A\d+\z/], Integer[0]] $memcachesize                 = $afs::params::memcachesize,
  Hash $options                                                         = $afs::params::options,
  Enum['on','off'] $runserver                                           = $afs::params::runserver,
  String $thiscell                                                      = $afs::params::thiscell,
  Boolean $prefer_upstream_dkms                                         = $afs::params::prefer_upstream_dkms,
  Boolean $enable_firewall                                              = $afs::params::enable_firewall,
  # Options only impacting C8 and newer.
  Boolean $aklog_unit                                                   = $afs::params::aklog_unit,
  Boolean $pam_afs_session                                              = $afs::params::pam_afs_session,
  Boolean $pam_afs_session_debug                                        = false,
  Boolean $handle_cellservdb                                            = $afs::params::handle_cellservdb,
  Boolean $use_osrepos                                                  = $afs::params::use_osrepos,
  Boolean $tools                                                        = false,
  Boolean $setcrypt                                                     = true,
) inherits afs::params {

  contain afs::install
  contain afs::config
  contain afs::service

  Class[afs::install] -> Class[afs::config] -> Class[afs::service]

  if $enable_firewall {
    contain afs::firewall
  }
}
