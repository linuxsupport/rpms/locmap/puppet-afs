# @summary Installs afs packages
# @api private
class afs::install(
  Boolean $prefer_upstream_dkms = $afs::prefer_upstream_dkms,
  Boolean $pam_afs_session = $afs::pam_afs_session,
  Boolean $aklog_unit = $afs::aklog_unit,
  Boolean $use_osrepos = $afs::use_osrepos,
  Boolean $tools = $afs::tools,
) {

  # Locmap code changes to support the desktop use-case
  # AFS changes things around
  $afs_arch_split = regsubst($::kernelrelease, '(\bx86_64\b|\baarch64\b)', '', 'G')
  $afs_kernel_release = regsubst($afs_arch_split, '-', '_', 'G')
  $afs_kmod_pkg = "kmod-openafs-*${afs_kernel_release}*"
  # End Locmap code changes

  if (versioncmp($facts['os']['release']['major'],'8') >= 0) and $use_osrepos {
    contain afs::kojitag
    $_afspkgs_require = 'Class[Afs::Kojitag]'
  } else {
    $_afspkgs_require = undef
  }

  Package {
    require => $_afspkgs_require,
  }

  case $facts['os']['release']['major'] {
    '7': { $afspkgs =  ['openafs', 'openafs-client', 'openafs-krb5', $afs_kmod_pkg, 'pam_krb5'] }
    default:   { $afspkgs =  ['openafs', 'openafs-client', 'openafs-krb5', $afs_kmod_pkg]}
  }

  package {$afspkgs:
    ensure  => present,
  }
  # Avoid that dkms-openafs might be installed as a possible dependency to openafs.
  Package[$afs_kmod_pkg] -> Package[openafs-client]
  Package[$afs_kmod_pkg] -> Package[openafs]

  if versioncmp($facts['os']['release']['major'],'8') >= 0 {

    $_aklog_unit_ensure = $aklog_unit ? {
      true    => 'present',
      default => 'absent',
    }
    package{'cern-aklog-systemd-user':
      ensure => $_aklog_unit_ensure,
    }

    $_pam_afs_session_ensure =  $pam_afs_session ? {
      true    => 'present',
      default => 'absent',
    }
    package{'pam_afs_session':
      ensure => $_pam_afs_session_ensure,
    }
  }

  package{'cern-wrappers':
    ensure => present,
  }

  if $tools {
    ensure_packages(['afs_tools'])
    # Avoid that afs_tools brings in dkms early.
    Package[$afs_kmod_pkg] -> Package['afs_tools']
  }

  # https://gerrit.openafs.org/#/c/15092/
  # hopefully will be gone with openafs-1.8.9 or later
  file{'/etc/sysconfig/modules/openafs-client.modules':
    ensure  => absent,
    require => Package['openafs-client'],
  }
}

