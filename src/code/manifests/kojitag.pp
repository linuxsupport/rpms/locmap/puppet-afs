# @summary Configure afs koji repositories
# @api private
#
class afs::kojitag {

  osrepos::kojitag{'openafs':
    available_major_versions => [8],
    priority                 => 30,
    description              => 'OpenAFS Packages',
    snapshots_available      => false,
  }
}

