Name:           puppet-afs
Version:        2.11
Release:        1%{?dist}
Summary:        Masterless puppet module for afs

Group:          CERN/Utilities
License:        BSD
URL:            http://linux.cern.ch
Source0:        %{name}-%{version}.tgz

BuildArch:      noarch

Requires:       puppet-stdlib
Requires:       puppet-agent

%if 0%{?rhel} >= 8
# cern-aklog-systemd-user requires kstart
Requires:   epel-release
Requires:   openafs-release
Requires:   puppet-selinux
%endif

%description
Puppet Afs module for locmap

%prep
%setup -q

%build
CFLAGS="%{optflags}"

%install
rm -rf %{buildroot}
install -d %{buildroot}/%{_datadir}/puppet/modules/afs/
install -d %{buildroot}/etc/locmap/code/environments/production/hieradata/modules/afs/
install -d %{buildroot}/etc/locmap/code/environments/production/hieradata/modules/afs/operatingsystems/
install -d %{buildroot}/etc/locmap/code/environments/production/hieradata/modules/afs/operatingsystems/RedHat/
cp -ra code/* %{buildroot}/%{_datadir}/puppet/modules/afs/
cp -ra data/afs.yaml %{buildroot}/etc/locmap/code/environments/production/hieradata/modules/afs/afs.yaml
touch %{buildroot}/%{_datadir}/puppet/modules/afs/linuxsupport

%files -n puppet-afs
/etc/locmap/code/environments/production/hieradata/modules/afs/afs.yaml
%{_datadir}/puppet/modules/afs
%doc code/README.md

%post
MODULE=$(echo %{name} | cut -d \- -f2)
if [ -f %{_datadir}/puppet/modules/${MODULE}/linuxsupport ]; then
  grep -qE "autoreconfigure *= *True" /etc/locmap/locmap.conf && AUTORECONFIGURE=1 || :
  if [ $AUTORECONFIGURE ]; then
    locmap --list |grep $MODULE |grep -q enabled && MODULE_ENABLED=1 || :
    if [ $MODULE_ENABLED ]; then
      echo "locmap autoreconfigure enabled, running: locmap --configure $MODULE"
      locmap --configure $MODULE || :
    else
      echo "locmap autoreconfigure enabled, however the $MODULE module is not enabled"
      echo "Skipping (re)configuration of $MODULE"
    fi
  fi
fi

%changelog
* Tue Oct 08 2024 Ben Morrice <ben.morrice@cern.ch> 2.11-1
- Add autoreconfigure %post script

* Wed Aug 23 2023 Ben Morrice <ben.morrice@cern.ch> 2.10-1
- use pam_afs_session on 8+

* Thu Aug 17 2023 Ben Morrice <ben.morrice@cern.ch> 2.9-1
- rebase to 5a94782a

* Tue Feb 28 2023 Marta Vila Fernandes <marta.vila.fernandes@cern.ch> 2.8-1
- Add afs support for aarch64 nodes

* Mon Jan 09 2023 Ben Morrice <ben.morrice@cern.ch> 2.7-1
- Rebase to 0f07e08588c58863
- Simplify deployment for all supported distributions

* Fri Dec 02 2022 Ben Morrice <ben.morrice@cern.ch> 2.6-3
- Bump release for disttag change

* Fri May 06 2022 Ben Morrice <ben.morrice@cern.ch> - 2.6-2
- ensure epel is present on el8+ (cern-aklog-systemd-user requires kstart)

* Wed Apr 20 2022 Ben Morrice <ben.morrice@cern.ch> - 2.6-1
- refactor RHEL support

* Mon Apr 04 2022 Ben Morrice <ben.morrice@cern.ch> - 2.5-1
- add RHEL support

* Mon Jan 10 2022 Ben Morrice <ben.morrice@cern.ch> - 2.4-1
- use DNS SRV records

* Tue Feb 23 2021 Ben Morrice <ben.morrice@cern.ch> - 2.3-2
- fix requires on puppet-agent

* Wed Feb 17 2021 Ben Morrice <ben.morrice@cern.ch> - 2.3-1
- change how kmod-openafs is installed

* Tue Feb 16 2021 Ben Morrice <ben.morrice@cern.ch> - 2.2-2
- add dektop specific code

* Fri Jan 15 2021 Ben Morrice <ben.morrice@cern.ch> - 2.2-1
- rebase to #0c72f5dd6

* Mon Feb 24 2020 Ben Morrice <ben.morrice@cern.ch> - 2.1-1
- rebase to c8 enabled code

* Mon Jan 27 2020 Ben Morrice <ben.morrice@cern.ch> - 2.0-4
- remove requires cern-afs-setserverprefs

* Thu Jan 09 2020 Ben Morrice <ben.morrice@cern.ch> - 2.0-3
- Build for el8

* Tue May 15 2018 Thomas Oulevey <thomas.oulevey@cern.ch> - 2.0-2
- Add requires cern-afs-setserverprefs

* Wed May 02 2018 Thomas Oulevey <thomas.oulevey@cern.ch> - 2.0-1
- New version for locmap 2.0

* Tue Mar 14 2017 Thomas Oulevey <thomas.oulevey@cern.ch> - 0.2-2
- Make CellServDB download optional

* Tue Nov 29 2016 Thomas Oulevey <thomas.oulevey@cern.ch> - 0.2-1
- Rebuild for 7.3 release

* Wed Nov 16 2016 Aris Boutselis <aris.boutselis@cern.ch>
- Authconfig is handling pam lines

* Fri Jun 10 2016 Aris Boutselis <aris.boutselis@cern.ch>
-Initial release


